//Ralph Haddad - CSE2 - 4/28/19
//RobotCity Program - hw10

import java.util.Random; //imports rng
public class RobotCity {
    
    public static int [][] buildCity() {
        Random rng = new Random();  //initiates rng
        int square = rng.nextInt(10) + 5;   //creates a random sized square from 10 to 15
        int [][] cityArray = new int [square][];    //allocates a new array with size square
        for (int col=0; col < square; col++) {
            cityArray[col] = new int[square];   //allocates for the columns based on square size
            for (int row=0; row < square; row++) {
                cityArray[col][row] = rng.nextInt(899) + 100;   //generates a number between 100-999 for each array member
            }
        }
        return cityArray;   //returns the final city array
    }

    public static void display(int [][] cityArray) {
        System.out.println();   //spacer
        for(int i = cityArray.length - 1; i >= 0; i--) {    //goes backwards throught the array so that array value 0 is at the bottom right
           for(int j = 0; j < cityArray.length; j++) {
               System.out.printf("%4d ", cityArray[j][i]);  //keeps space for a negative and the three random integer values
           }
           System.out.println("\n");    //spacer
       } 
    }
    
    public static void invade(int [][] cityArray, int k) {
        for (int i = 0; i < k; i++) {   //creates k amount of negatives
            Random rng = new Random();  //initiates rng
            int x = rng.nextInt(cityArray.length);  //creates an x value for the array
            int y = rng.nextInt(cityArray.length);  //creates a y value for the array

            if (cityArray[x][y] < 0) {  //ueses x, y values to ensure k amount of negatives in the array at random
                i--;    //when that location already has a negative, reiterates the loop for a new random value
                continue;   //exits the if statement
            } else {
                cityArray[x][y] = -1 * cityArray[x][y]; //creates a negative at x,y point if positive
            }   
        }
    }

    public static void update(int [][] cityArray) { //moves the negatives west to east
        for (int col = cityArray.length-1; col >= 0; col--) {   
            for (int row = 0; row < cityArray.length; row++) {
                if (cityArray[col][row] < 0) {  
                    cityArray[col][row] = -1 * cityArray[col][row]; //checks the array for postive values and sets them as negative
                    if (col == cityArray.length - 1) {  //checks end case on the east edge and passes on to the next loop
                        continue;
                    }
                    cityArray[col+1][row] = -1 * cityArray[col+1][row]; //sets the next value as a negative 
                }
            }
        }
    }

    public static void main(String[] args) {
        Random rng = new Random();  //initiates rng
        int [][] cityArray = buildCity();   //creates the city
        display(cityArray); //displays the city initially
        invade(cityArray, rng.nextInt(15) + 5);   //sets the robots from 5 to 20
        display(cityArray); //displays the city with the robots

        for (int iteration = 1; iteration <= 5; iteration++) {  //goes through the waves of the robots moving
            System.out.println("Wave: " + iteration);   //prints the wave
            update(cityArray);  //updates the array
            display(cityArray); //dusplays the array
        }
    }
}
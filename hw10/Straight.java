//Ralph Haddad - CSE2 - 4/28/19
//Straight program - hw10
import java.util.Random;
import java.util.Arrays;
public class Straight {
    
    public static void Shuffle(int [] deck) {
        Random rng = new Random();  //initializes rng
        for (int i=0; i < 52; i++) {
            deck[i] = i;    //generates values for the array
        }
        for (int i = 0; i < 52; i++) { //swaps cards at random
            int temp = rng.nextInt(51) + 1; //ramdomizes a location in the deck
            int newv = deck[i]; //shuffles the cards
            deck[i] = deck[temp];
            deck[temp] = newv; 
        }
    }
    
    public static int [] Draw(int [] deck) {
        int [] hand = new int [5];  //initializes and allocates an array for a hand of 5
        for (int i=0; i < 5; i++) {
            hand[i]=deck[i];    //takes the first 5 values from the deck for the hand
        }
        return hand;    //returns top 5 cards from the deck
    }

    public static int Search(int [] hand, int k) {
        int temp;
        if (k > 5 || k <=0) {   //checks for out of bounds
            return -1;  //returns stating k value is invalid
        } else {
            for(int i = 0; i<hand.length-1; i++) {
                for(int j = hand.length-1; j > 0; j--) {
                    if (hand[j]<hand[j-1]) {    //swaps values based on which is lower, up to 5 times
                        temp = hand[j];
                        hand[j] = hand[j-1];
                        hand[j-1] = temp;
                    }
                }
            }
        }
        int l = hand[k-1];  //returns the k location of the lowest value
        return l;   //will return between 1-5 of the lowest value
    }

    public static int StraightSearch(int [] hand) { //method to search for straight
        int [] tempArray = new int [5]; //creates array to hold the five cards
        for (int i=0; i<5; i++) {
            tempArray[i] = Search(hand, (i+1)); //takes other array to find the lowest value
        }
        if (tempArray[1]== (tempArray[0] +1) && tempArray[2] == (tempArray[1] + 1) && tempArray[3] == (tempArray[2]+1) && tempArray[4] == (tempArray[3]+1)) {
            return 1;   //returns 1 if it meets the requirement of a straight
        } 
        return 0;   //returns 0 if not ascending
    }

    public static void main(String[] args) {
        double sum = 0; //initializes the sum of straights
        int straight;   //number to be returned as either 1 or 0
        int [] hand;  //generates the arrays for the deck and hand
        int [] deck = new int [52]; //allocates an array for a deck once
        double test = 1000000;  //times a straight is tested for
        for (int i = 0; i < test; i++) {
            Shuffle(deck);  //generates the deck and shuffles the values
            hand = Draw(deck);  //draws the top 5 cards
            straight = StraightSearch(hand);    //checks the hand for a straight
            sum += straight;    //adds on to the sum of straights if sucessful
        }
        System.out.println("The program drew one million decks and found " + (int)sum + " straights!");
        System.out.println("The percentage frequency of straights was: ");
        System.out.printf("%1.4f", ((sum/test)*100));   //prints percentage frequency
        System.out.println("%");

    }
}
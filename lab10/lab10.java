//Ralph Haddad - CSE2 - 4/19/19
//Matrix lab, lab 10, Chen
import java.util.Random;//imports rng
public class lab10 {
    public static int [][] increasingMatrix(int width, int height, boolean format) {
        if (format) {   //if format is true, generate row-major array 
            int [][] rowM = new int [height][];  //sets number of rows based on height
            int counter = 1;    //sets a counter to follow location on matrix
            for (int row=0; row < height; row++) {
                rowM[row] = new int[width]; //allocates for the row with given width
                for (int col=0; col < width; col++) {
                    rowM[row][col] = counter;    //starts the array at 1 and goes up 
                    counter++;
                } 
            }
            return rowM;
        } else {        //if format is false, generate column-major array
            int [][] colM = new int [width][];  //sets number of columns based on width
            int counter = 1;
            int sum = 0;
            for (int col=0; col < width; col++) {
                colM[col] = new int[height];    //initializing columns in matrix based on height to be allocated
                for (int row=0; row < height; row++) {
                    colM[col][row] = counter + sum;
                    sum += width;
                }
                counter++;
                sum = 0;
            }
            return colM;
        }
    }

    public static void printMatrix(int [][] array, boolean format) {
        if (format == true) {    //will print for row-major matrix
            for (int row=0; row < array.length; row++) {    //FILLS VALUES A ROW AT A TIME
                for (int col=0; col < array[0].length; col++) {
                    System.out.print(array[row][col] + " ");
                }
                System.out.println();
            }
        } 
        else if (format == false) {    //will print for column-major matrix
            for (int i=0; i < array[0].length; i++) {    //FILLS VALUES A COLUMN AT A TIME
                for (int j=0; j < array.length; j++) {
                    System.out.print(array[j][i] + " ");
                }
                System.out.println();
            }
        } else if (array == null) {
            System.out.println("The array was empty!"); //when input array is null
        }
    }

    public static int[][] translate(int [][] colMajorMatrix) {  //takes in a column major matrix to reformat into row major 
        int [][] newRowM = new int [colMajorMatrix[0].length][];    //creates a row major array with the columns of the old array
        for (int row=0; row < colMajorMatrix[0].length; row++) {    //creates a row-major matrix based on the vaules of the column-major matrix
            newRowM[row] = new int [colMajorMatrix.length];
        }
        for (int row = 0; row < newRowM.length; row++) {
            for (int col=0; col < newRowM[row].length; col++) {
                newRowM[row][col] = colMajorMatrix[col][row];   //NOT A TRANSPOSE -> takes the column major matrix values as a reflection of the row major matrix
            }
        }
        return newRowM; //returns the new row major matrix
    }

     public static int [][] addMatrix(int [][] a, boolean formata, int [][] b, boolean formatb) {
    
        if (formata == false) {
            a = translate(a);
        } else if (formatb == false) {
            b = translate(b);
        }
        if(a.length!=b.length || a[0].length!=b[0].length) {
            System.out.println("The arrays cannot be added! Expect an error below :(");
            return null;
        }
        //completes the translation at this point to have a and b as row-major
        int [][] apb = new int[a.length][]; //creates a new matrix to add a and b
        for (int row=0; row < a.length; row++) {
            apb[row] = new int [a[0].length];  //allocates the width which is the same as the length
            for (int col=0; col< a[0].length; col++) {
                apb[row][col] = a[row][col] + b[row][col];
            }
        }
        return apb;
     } 

    public static void main(String[] args) {
        Random rng = new Random();  //defines random number generator
        int height1 = rng.nextInt(4)+1;   //generates 2 random widths and heights
        int height2 = rng.nextInt(4)+1;
        int width1 = rng.nextInt(4)+1;
        int width2 = rng.nextInt(4)+1;
        System.out.println("MatrixA: ");
        printMatrix(increasingMatrix(width1, height1, true), true);   //prints matrix A
        System.out.println("MatrixB: ");
        printMatrix(increasingMatrix(width1, height1, false), false);  //prints matrix B
        System.out.println("Matrix A + B: ");
        printMatrix(addMatrix(increasingMatrix(width1,height1,true), true, increasingMatrix(width1,height1, false), false), true);  //prints A + B
        System.out.println("Matrix A + C: ");
        printMatrix(addMatrix(increasingMatrix(width1,height1,true), true, increasingMatrix(width2,height2, false), false), true);  //prints A + C
    }
}
//Ralph Haddad - CSE2 - 4/15/19
//ArrayGames program, hw09

import java.util.Arrays;    //imports arrays
import java.util.Random;    //imports random
import java.util.Scanner;   //imports scanner
public class ArrayGames {
    public static int[] generate() {
        Random rgen = new Random();     //initializes rng
        int [] array = new int[rgen.nextInt(10)+10];     //creates an 
        for (int p = 0; p < array.length; p++) {
            array[p] = rgen.nextInt(50);   //stores 5 numbers between 0 and 59 in array  
        }
        return array;
    }

    public static void print(int [] array) {
        System.out.println("");
        for (int i = 0; i < array.length; i++){     //goes through any given array and prints each value up to array length
                System.out.print(array[i] + " ");
        }   System.out.println("");
    }

    public static int[] insert(int [] arr1, int [] arr2) {
        Random rgen = new Random();     //initializes rng
        int newLength = arr1.length + arr2.length;  //creates a new array the size of arr1 and arr2
        int insert = rgen.nextInt(arr1.length); //determines the insertion point randomly
        int [] arrTot = new int[newLength];
        for (int i=0; i<insert; i++) {  //sets the new array as the first array until the insertion point
            arrTot[i]=arr1[i];
        }
        for (int i=0; i<arr2.length; i++) { //loop which inserts the second array starting at insert value
            arrTot[i+insert]=arr2[i];      
        }   
        for (int i=insert+arr2.length; i<newLength; i++) {  //loop which counts up starting from inserted point and 
            arrTot[i]=arr1[i-arr2.length];                  //following the insertion of the second array, redefines new array
        }
        return arrTot;  //returns the resulting array
    }

    public static int [] shorten(int [] array, int imput) {
        int [] newArr = new int[array.length-1];    //creates a new array one less than the original
        if (imput > array.length || imput < 0) {    
            return array;   //if the member inputted is outside the array length
        } else {
            for (int i=0; i<imput; i++) {
                newArr[i] = array[i];   //counts up and sets the new array up to the value to be skipped
            }
            for (int i=imput; i<newArr.length; i++) {
                newArr[i] = array[i+1]; //skips the value the user inputted when making the new array
            }
        }
        return newArr;  //returns the array
    }
    
    public static void main(String[] args) {
        Scanner myIn = new Scanner (System.in); //initializes the scanner class as myIn(put)
        String entry = "";
        int i = 1;  //sentinel variable to see if the program can proceed
        System.out.println("Please enter insert or shorten: ");    //initial print statement request
        do {    //a do while loop that does not let the program proceed until a correct answer is given
            entry = myIn.next();    //initial request to take in a string
            while (i!=0) {  //repeats as long as the sentinel variable is 1
                if ((entry.equals("shorten")) || (entry.equals("insert"))) {    //checks if any of the strings matches the input
                    i = 0;
                    break;   //skips the else and while statement 
                } else {
                    System.out.println("Nope. Please enter insert or shorten: ");  //printed when anything but a shape is used
                    entry = myIn.next();    //clears shape and requests a new entry
                }
            }
        } while (i != 0);   //once i is 0, program proceeds with the inputted string for shape
        System.out.println(">>Input " + entry + " recieved!");  //confirmation that a correct string shape is recieved
        
        if (entry.equals("shorten")) {
            int [] arr1 = generate();   //generates the array for the shorten method to use
            System.out.print("Input1: ");   
            print(arr1);    //prints the array before asking for what to be removed
            int member; //initializes the member to be removed
            do {    //pareses user input to ensure an integer   
                System.out.print("Enter member integer of array to remove: ");
                while (!myIn.hasNextInt()) {  //when no integer is placed, requests an integer and junks the last input
                    System.out.print("That is not an integer, enter again: ");
                    myIn.next(); 
                }
                member = myIn.nextInt();  //sets integer width
                System.out.println("Input2: " + member);    //prints the user input
                System.out.print("Output: ");
                print(shorten(arr1, member));   //runs shorten method with array and user input, then prints it
            } while (member < 0); //ensures a value greater than 0
        } else {    //when user enters insert
            int [] arr1 = generate();       //generates first array
            int [] arr2 = generate();       //generates second array
            System.out.print("Input1: "); print(arr1);  //prints the first array
            System.out.print("Input2: "); print(arr2);  //prints the second array
            System.out.print("Output: ");   //runs the insert and print method with both arrays
            print(insert(arr1, arr2));
        }
    }
}
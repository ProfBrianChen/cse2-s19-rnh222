//Ralph Haddad - CSE2 - lab09
//Search Program Practice

import java.util.Scanner;   //imports scanner class
import java.util.Random;    //imports random generator
import java.util.Arrays;    //imports arrays class
public class lab09 {
    
    public static int[] ranArray() {
        Random rgen = new Random();     //initializes rng
        int [] array = new int[20];     //creates an 
        for (int p = 0; p < 20; p++) {
            array[p] = rgen.nextInt(50);   //stores 5 numbers between 0 and 59 in array  
        }
        return array;   //returns the resulting array
    }

    public static int[] ranAscending(int [] array) {    
        Arrays.sort(array); //sorts the array in ascending order
        return array;   //returns the array
    }

    public static int linearSearch(int [] array, int find) {    //linear search method
        for (int i = 0; i<array.length; i++){   //goes up the array and checks if the value given is in the array
            if (array[i]==find){
                return i;   //gives the location in the array the value is found
            }
        }
        return -1;  //if no value is found, returns -1
    }

    public static int bianarySearch(int array[], int find) {    //bianary search method
        int l = 0;  //initiates start of array search
        int r = array.length - 1;   //initiates end of array search
        while (l <= r){ //ensures the search does not overlap itself
            int mid = (l + (r-l))/2;  //finds the midpoint of the two vales l and r
            if (array[mid]==find) {return mid;} //if the midpoint equals the value to be found, returns the midpoint
            if (array[mid]<find) {  //checks the upper half of the array
                l = mid + 1;
            } else if (array[mid]>find) {   //checks the lower half of the array
                r = mid - 1;
            }
        }
        return -1;  //if value isnt found, returns -1
    }
    
    public static void main(String[] args) {    //begins main method
        Scanner myIn = new Scanner(System.in);  //initializes scanner with myIn
        
        int imput;  //initilizes the input
        System.out.print("Enter an 0 for linear search and 1 for bianary search: ");    //requests what to be done
        imput = myIn.nextInt(); //takes user input 
        if (imput == 0) {   //if linear search...
            int [] array = ranArray();  //creates an array
            System.out.print("Enter a value between 0 and 50 to find: ");   //takes user input
            int userIn = myIn.nextInt();
            System.out.print("Result: " + linearSearch(array, userIn)); //runs linear search method

            System.out.println("");
            for (int i = 0; i < 20; i++){   //prints the array for user to check
                System.out.print(array[i] + " ");
            }   System.out.println("");
        } else if (imput == 1) {    //if bianary search...
            int [] array = ranAscending(ranArray());    //creates a sorted array
            System.out.print("Enter a value between 0 and 50 to find: ");   //takes sorted input
            int userIn = myIn.nextInt();
            System.out.print("Result: " + bianarySearch(array, userIn));    //prints the result of the search

            System.out.println("");
            for (int i = 0; i < 20; i++){   //prints the array for user to check
                System.out.print(array[i] + " ");
            }   System.out.println("");
        }
    }//end of main method
}//end of class
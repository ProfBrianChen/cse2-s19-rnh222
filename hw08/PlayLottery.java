//Ralph Haddad - CSE2 - 4/7/19
//PlayLottery Program, hw08
//A lottery game where the user inputs 5 different integers to be taken in as an array
//then creates a random series of 5 integers between  0 and 59, then compares the nubers
//in order to the random numbers to see if there is a match
import java.util.Random;    //imports random generator
import java.util.Scanner;   //imports scanner
public class PlayLottery {  //main class
    
    public static int[] numbersPicked() {   //creates random non-repeating numbers to be matched by the user
        Random rgen = new Random();     //initializes rng
        int [] WinningNum = new int[5]; //initializes an array of 5 winning numbers
        System.out.print("The winning numbers are: ");
        for (int p = 0; p < 5; p++) {
            WinningNum[p] = rgen.nextInt(59);   //stores 5 numbers between 0 and 59 in array  
        }
        for (int i=0; i<5; i++){    //checks each number for duplication
            for (int j=0; j<5; j++) {
                while(WinningNum[i] == WinningNum[j] && i != j) {   //if there is a repeat, reassigns a new number
                    WinningNum[i] = rgen.nextInt(59);
                }
            }
            System.out.print(WinningNum[i] + " ");  //eventually prints valid numbers
        }   System.out.println("");
        return WinningNum;  //retuns array of winning numbers, must be stored 
    }

    public static boolean userWins(int[] user, int[] winning) { //compares winning numbers with user input in order
        int match = 0;  //counts up the number of matching numbers in order only
        for (int i = 0; i<5; i++) {
            if (user[i]==winning[i]) {
                match++;
            }
        }
        if (match == 5) {   //retuns true if all numbers match in order
            return true;
        } else {
            return false;
        }

    }
    
    public static void main(String[] args) {    //main method
        Scanner myIn = new Scanner(System.in);  //initializes scanner under myIn
        Random rgen  = new Random();            //initializes random number generator
        
        int [] UserIn= new int[5];
        for (int i = 0; i < 5; i++) {
            do {   
            System.out.print("Enter number " + (i+1) + " of 5 between 0 and 59: ");
            while (!myIn.hasNextInt()) {  //when no integer is placed, requests an integer and junks the last input
                System.out.print("That is not an integer, enter again: ");
                myIn.next(); 
            }
            UserIn[i] = myIn.nextInt();  //sets integer width
            } while ((UserIn[i] < 0) || (UserIn[i] > 59)); //ensures a value between 0 and 59
            System.out.println("  > Valid Number Input: " + UserIn[i]); //confirms the input was taken for the pattern
        }
        
        System.out.print("Your lottery choices are: "); //lists the users numbers
        for (int j = 0; j < UserIn.length; j++) {
            System.out.print(UserIn[j] + " "); 
        }   System.out.println("");

        int [] WinNum = numbersPicked();    //set's an array with the result of numbers picked
        boolean result = userWins(UserIn, WinNum);  //creates a boolean result from a compasison of the two arrays
        //prints the final result based on the previous method return
        if (result) {System.out.println("Wow you won, that's like impossible.");} else {System.out.println("You lost, it's normal.");}
    }//end of main method
}   //end of main class

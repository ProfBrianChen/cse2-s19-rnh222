//Ralph Haddad - CSE2 - 4/7/19
//Letters Program, hw08
//Creates an array of arbitrary size filled with random characters
//the program then seperates the first array into two arrays
//sorting letters A-M and N-Z inclusive. Outputting the new sorted arrays.
import java.util.Arrays;
import java.util.Random;
public class Letters {  //main class
    
    public static void getAtoM(char[]alphBox) {  //sorts for letters A to M inclusive
        int sizeAtoM = 0;
        for (int p = 0; p < alphBox.length; p++) {  //parses alphBox for characets to make a correctly sized array
            if ((alphBox[p] <= 'M' && alphBox[p] >= 'A') || (alphBox[p] >= 'a' && alphBox[p] <= 'm')) {
                sizeAtoM += 1;
            }
        }

        char[]arrayAtoM = new char[sizeAtoM];   //declares the new array with correct size
        int nextIn = 0; //used in order to add and move up the array arrayAtoM if specifications are met
        for (int q = 0; q < alphBox.length; q++) {
            if ((alphBox[q] <= 'M' && alphBox[q] >= 'A') || (alphBox[q] >= 'a' && alphBox[q] <= 'm')) {
                arrayAtoM[nextIn] = alphBox[q];
                nextIn++;  
            }
        }

        System.out.print("A to M character array: ");   //Prints out the resulting list of characters
        for (int a = 0; a < arrayAtoM.length; a++) {
            System.out.print(arrayAtoM[a]);
        }   System.out.println("");

    }

    public static void getNtoZ(char[]alphBox) {  //sorts for letters N to Z inclusive, operator change is all that is done
        int sizeNtoZ = 0;
        for (int p = 0; p < alphBox.length; p++) {  //parses alphBox for characets to make a correctly sized array
            if ((alphBox[p] >= 'N' && alphBox[p] <= 'Z') || !(alphBox[p] >= 'n' && alphBox[p] <= 'z')) {
                sizeNtoZ += 1;
            }
        }

        char[]arrayNtoZ = new char[sizeNtoZ];   //declares the new array with correct size
        int nextIn = 0; //used in order to add and move up the array arrayAtoM if specifications are met
        for (int q = 0; q < alphBox.length; q++) {
            if ((alphBox[q] >= 'N' && alphBox[q] <= 'Z') || (alphBox[q] >= 'n' && alphBox[q] <= 'z')) {
                arrayNtoZ[nextIn] = alphBox[q];
                nextIn++;  
            }
        }

        System.out.print("N to Z character array: ");
        for (int a = 0; a < arrayNtoZ.length; a++) {
            System.out.print(arrayNtoZ[a]);
        }   System.out.println("");
    }
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~This is where the magic comes together~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public static void main(String[] args) {    //main method
        Random rgen = new Random();     //random number generator

        char[]alphBox = new char[25];  //declares the array and allocates it between 0 and 25
        String Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";   //string of the alphabet to be used

        for(int i = 0; i < alphBox.length; i++) {
            alphBox[i] = Alphabet.charAt(rgen.nextInt(52));   //takes a random letter from the alphabet including capials to be stored in the array
        }

        System.out.print("Random character array: ");//Prints initial statement
        for(int j = 0; j < alphBox.length; j++) {   //prints the entire array
            System.out.print(alphBox[j]);       
        }   System.out.println("");     //moves down a line

        getAtoM(alphBox);   //runs getAtoM using the created array
        getNtoZ(alphBox);   //runs getNtoZ using the created array
        
        System.out.println("");
        

    }   //end of main method
}   //end of main class 
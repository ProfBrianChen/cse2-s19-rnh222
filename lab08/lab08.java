//Ralph Haddad - CSE2 - Array Lab - 4/5/19
//Array program that creates an array of size 50 to 100
//and populates it with random integers from 0 to 99
//to then find the range, mean , and standard deviation
//After the range is found by sorting the array, array needs
//to be shuffled afterwards.
import java.lang.Math;
import java.util.Arrays;
import java.util.Random;
public class lab08 {

    public static void getRange(int [] rRange) { //range finding method
        // int index = 0;      //finds minimum value
        // int min = rRange[0];
        // for (int i = 1; i<rRange.length; i++) {
        //     if (rRange[i] < min) {
        //         min = rRange[i];
        //         index = i;
        //     }
        // }
        // int index1 = 0;     //finds maximum value
        // int max = rRange[0];
        // for (int j = 1; j<rRange.length; j++) {
        //     if (rRange[j] > max) {
        //         max = rRange[j];
        //         index = j;
        //     }
        // }
        // int range = max - min;
        // System.out.println("Range is: " + range);
        
        Arrays.sort(rRange);
        int range = rRange[rRange.length-1]-rRange[0];
        System.out.println("Range is: " + range);  //prints the range following arrays.sort method
    }

    public static int getMean(int [] array) {
        int sum=0;
        for (int i = 0; i < (array.length); i++) {
            sum = sum + array[i]; 
        }
        int mean = sum / (array.length);    //finds the mean
        System.out.println("The mean is: " + mean);
        return mean;
    }

    public static void getStdDev(int [] array, int mean) {    //finds the standard deviation
        double sum = 0;
        for (int i = 0; i < (array.length); i++) {
            sum = sum + Math.pow((array[i] - mean), 2); 
        }
        double StdDev = Math.sqrt((sum)/(array.length - 1));
        System.out.println("The Standard Deviation is: " + StdDev);
    }

    public static void shuffle(int [] array) {  //shuffles the array 
        Random rgen = new Random();     //random number generator
        for (int i = 0; i < array.length; i++) {    //assigns a random integer from 0 to 99 for each array
            int RanPos = rgen.nextInt(array.length);
            int temp = array[i];                    //takes an array point, puts it in a temp
            array[i] = array[RanPos];               //and swaps it with another random point in the array
            array[RanPos] = temp;
            System.out.println(array[i]);
        }

    }

    public static void main(String[] args) {

        int rRange = (int) (Math.random() * ((100-50)+1)) + 50; //creates a random integer between 50 and 100 inclusive

        int[] numSet; //declares an array munSet
        numSet = new int[rRange];   //creates an array with allocated size rRange

        for (int i = 0; i < rRange; i++) {    //assigns a random integer from 0 to 99 for each array
            numSet[i] = (int)(Math.random() * 100);
            System.out.println(numSet[i]);
        }

        getRange(numSet);           
        int mean = getMean(numSet); //returns and sets a value of mean to be used in StdDev
        getStdDev(numSet, mean);
        shuffle(numSet);

    }    
} 
// Ralph Haddad - CSE2 - 3/25/19 - Area assignment Hw07
//A program that can calculate the area of a rectacgle, triangle, or circle

// (!shape.equals("rectangle")) | (!shape.equals("triangle")) | (!shape.equals("circle"))

import java.util.Scanner;   //imports scanner class

public class Area {

    public static double triangle(double l, double h) {
        double A = (.5)*l*h;
        System.out.println("Triangle has area of: " + A + " units.");
        return A;
    }

    public static double circle(double r) {
        double A = 6.28*(r*r);
        System.out.println("Circle has area of: " + A + " units.");
        return A;
    }

    public static double rectangle(double l, double h) {
        double A = l*h;
        System.out.println("Rectangle has area of: " + A + " units.");
        return A;
    }

    public static double check() {
        Scanner myIn = new Scanner (System.in); //initializes the scanner class as myIn(put) within check method
        double result;//initeates what will be returned
        do {  //a do while loop to ensure a value capable of being uesd in the 
            System.out.print("Enter an integer: ");
              while (!myIn.hasNextDouble()) {  //when no integer is placed, requests an integer and junks the last input
                System.out.print("That is not a double, please re-enter: ");
                myIn.next(); 
              }
              result = myIn.nextDouble();  //sets integer width
            } while (result <= 0); //ensures a value between 1 and 10 by refusing all other values
            System.out.println("  > Valid Doule Input: " + result); //confirms the input was taken
        return result; 
    }

    public static void main(String[] args) {
      
        Scanner myIn = new Scanner (System.in); //initializes the scanner class as myIn(put)
        String shape = "";   //initializes the string shape
        
        int i = 1;  //entinel variable to see if the program can proceed
        System.out.println("Please enter rectangle, triangle, or circle: ");    //initial print statement request
        do {    //a do while loop that does not let the program proceed until a correct answer is given
            shape = myIn.next();    //initial request to take in a string
            while (i!=0) {  //repeats as long as the sentinel variable is 1
                if ((shape.equals("rectangle")) || (shape.equals("triangle")) || (shape.equals("circle"))) {    //checks if any of the strings matches the input
                    i = 0;
                    break;   //skips the else and while statement 
                } else {
                    System.out.println("Nope. Please enter rectangle, triangle, or circle: ");  //printed when anything but a shape is used
                    shape = myIn.next();    //clears shape and requests a new entry
                }
            }
        } while (i != 0);   //once i is 0, program proceeds with the inputted string for shape
        System.out.println(">>Input " + shape + " recieved!");  //confirmation that a correct string shape is recieved

        if (shape.equals("rectangle")) {
            System.out.print("Length entry. ");
            double l = check();
            System.out.print("Height entry. ");
            double h = check();
            rectangle(l , h);
        } else if (shape.equals("triangle")){
            System.out.print("Length entry. ");
            double l = check();
            System.out.print("Height entry. ");
            double h = check();
            triangle(l , h);
        } else if (shape.equals("circle")) {
            System.out.print("Radius entry. ");
            double r = check();
            circle(r);
        }

    }//end of main method
}//end of main class

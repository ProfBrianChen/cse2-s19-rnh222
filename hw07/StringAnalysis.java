//Ralph Haddad - CSE2 - 3/26/19 - String Analysis
//Analyses strings for something other than characters
//based on user input.

import java.util.Scanner; //imports scanner class

public class StringAnalysis {
    public static boolean fullIn (String a, int b) {    //method for int imput
        boolean phrases = true;   //when there is a string present
        int i = 0;
        if ( b < a.length()) {  //for when length entered is less than the entire length
            for (int j = 0; j < b; j++) {   //length entered
                char phrase = a.charAt(j);   //looks at position j
                if (phrase > 'z' || phrase < 'a') {
                    i++;    //increments i by 1
                } 
            }

        } else { //if length is longer than total length
            for (int j = 0; j < a.length(); j++) {
                char phrase = a.charAt(j);  //checks each character in phrase
                if (phrase > 'z' || phrase < 'a')   //checks char is not lowercase
                i++;    //increments i by 1
            }
        }

        if (i != 0) {
            phrases = false;     //phrase is false
        }
        return phrases;
    } // end of String method

    public static boolean fullIn ( String a) {  //runs when no int
        int p = 0;  //initializes counter p
        boolean phrases = true;  //initializes phrase
        for (int j = 0; j < a.length(); j++) {
            char phrase = a.charAt(j); //checks for each character throughout the length of the phrase
            if (phrase > 'z' || phrase < 'a') {
                p++;    //increase p by 1
            }
        }
        if (p != 0) {   //when i is not 0, phrase is false
            phrases = false; 
        }
        return phrases;  //allows phrase down to main method
    }

    public static void main(String[] args) {    //public main method
        Scanner myIn = new Scanner (System.in); //scanner to take in inputs
        
        int q = 1;  //initializes q counter
        int val = 0;    //initializes value integer
        boolean sat = true; //initializes a completeion boolean

        System.out.print("Input string: "); //requests input 
        String input = myIn.nextLine(); //takes in string
        System.out.print("Will whole string be analyzed? (y/n): ");   //requests action taken
        String full = myIn.nextLine();  //takes full anser

        do {    // main while loop to determine based on y/n asnwer how to proceed with analysis
            if (full.equals("n")) { //if answer was no
                System.out.print("How many characters should be analyzed?: ");
                while (!myIn.hasNextInt() || val <=0) {
                    if (myIn.hasNextInt()){
                        val = myIn.nextInt();   //takes in integer as val
                        if (val > 0) {  //if val is between 1 and 10
                            break;  //leaves loop
                        }
                        val=0;  //reset for val  
                    } else {    //if not int
                        String junkSecond = myIn.next();    //discards input
                    }
                    System.out.println("Please enter positive int");    //requests pos int
                }
                sat = fullIn(input, val);   //checks for satisfaction until a value
                q--;    //exits while loop
            } else if (full.equals("y")){
                sat = fullIn(input);    //checks to satisfy if the input has anything other than a string
                q--;    //exits while loop
            } else {//when user does not answer y/n
                int p = 1;  //exit variable
                while (p != 0) {    //loops until y or n is inputted
                    if (full.equals("y") || full.equals("n")) {
                        p = 0;
                    } else {
                        System.out.println("Please enter (y/n).");   //prints error statement
                        full = myIn.next(); //sets full as next input
                    }
                }
            }
        } while (q == 1);  //end of main while loop based on y/n input

        if (sat == true) {  //if everything is satisfied
            System.out.println("Every character is a letter");
        } else {    //if not satisfied
            System.out.println("Every character is not a letter");
        }
    }//end of main method

}//end of class
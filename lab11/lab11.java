//Ralph Haddad - CSE2 - 5/3/19
//lab11 - Creating a sorting method
import java.util.Arrays;    //imports arrays
public class lab11 {
    public static void main(String[] args) {
        int [] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int [] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        System.out.println();
        int iterBest = selectionSort(myArrayBest);
        System.out.println();
        int iterWorst = selectionSort(myArrayWorst);
        System.out.println("The total number of operations performed on the sorted array : " + iterBest);
        System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
    }

    public static int selectionSort(int[] list) {
        int iter = 0;
        for (int i = 0; i < list.length-1; i++) {
            int currentMinIndex = i;
            System.out.println(Arrays.toString(list));
            for (int j = i + 1; j < list.length; j++) {
                if (list[j]<list[currentMinIndex]) {
                    currentMinIndex=j;
                }
                iter++;
            } 
            if (currentMinIndex !=i) {
                int temp = list[currentMinIndex];
                list[currentMinIndex] = list[i];
                list[i] = temp;
            }
            iter++;
        }
        return iter;    //returns the number of iterations 
    }

}
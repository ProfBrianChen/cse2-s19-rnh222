//Ralph Haddad - Lab07 - CSE2
//Creates artificial sentences into a paragraph
//containing a theis, action sentences, and a conclusion

public class lab07 {    //intiates the class
    public static String adj (String a) { //creates a method that uses a random number generator to assign an adjective from a switch collection
    int x = (int) (Math.random()*10);
    switch (x) {
      case 0:
        a = "bombastic";
        break;
      case 1:
        a = "inticing";
        break;
      case 2:
        a = "catastrophic";
        break;
      case 3:
        a = "beautiful";
        break;
      case 4:
        a = "clean";
        break;
      case 5:
        a = "muscular";
        break;
      case 6:
        a = "defeatedly";
        break;
      case 7:
        a = "large";
        break;
      case 8:
        a = "small";
        break;
      case 9:
        a = "miniscule";
        break;
      default:
        a = "";
    }
    return a;
  }
    public static String SubjectN (String a) {  //creates a method that uses a random number generator to assign a subject from a switch collection
    int x = (int) (Math.random()*10);
    switch (x) {
      case 0:
        a = "Horse";
        break;
      case 1:
        a = "Walrus";
        break;
      case 2:
        a = "Person";
        break;
      case 3:
        a =  "Gundam";
        break;
      case 4:
        a = "Dog";
        break;
      case 5:
        a = "Owl";
        break;
      case 6:
        a = "Alien";
        break;
      case 7:
        a = "Space-whale";
        break;
      case 8:
        a = "Extradimentional being";
        break;
      case 9:
        a = "Pokemon";
        break;
      default:
        a = "";
    }  
    return a;
  }
    public static String PastTenseV (String a) {  //creates a method that uses a random number generator to assign a past tense verb from a switch collection
    int x = (int) (Math.random()*10);
    switch (x) {
      case 0:
        a = "accepted";
        break;
      case 1:
        a = "jumped";
        break;
      case 2:
        a = "brushed";
        break;
      case 3:
        a = "compressed";
        break;
      case 4:
        a = "dabbed";
        break;
      case 5:
        a = "dipped";
        break;
      case 6:
        a = "dodged";
        break;
      case 7:
        a = "dived";
        break;
      case 8:
        a = "drank";
      case 9:
        a = "provided";
        break;
      default:
        a = "";
    }  
    return a;
  }
    public static String obj (String a) {         //creates a method that uses a random number generator to assign an object from a switch collection
    int x = (int) (Math.random()*10);
    switch (x) {
      case 0: 
          a = "car";
          break;
      case 1:
          a = "airplane";
          break;
      case 2:
          a = "mountain";
          break;
      case 3:
          a = "White-House";
          break;
      case 4:
          a = "house";
          break;
      case 5:
          a = "tesseract";
          break;
      case 6:
          a = "portal";
          break;
      case 7:
          a = "plant";
          break;
      case 8:
          a = "quiche";
          break;
      case 9:
          a = "dorm";
          break;
      default:
          a = "";
    }  
    return a;
  }
 
  
  public static String thesis() {     //thesis method that assigns the subject and uses it in the thesis sentence and returns it out of the method
    String subjectThesis = SubjectN("");
    System.out.println("The " + adj("") + " " + subjectThesis + " " + PastTenseV("") + " " + "the " + adj("") + " " + obj("") + ".");
    return subjectThesis;
  }

  public static void actionSeq(String subjectThesis) {  //initiates the action sequence method for the body paragraph
    double rng = Math.random();                         //creates a random number generator
    String newformSubj = "";                            //creates an empty string to be later assigned in the method
    if (rng < 0.5) {          //uses random number generator that has a half chance of either using "It" or the subject itself to be assigned to the sentence
      newformSubj = "It";
    }  else {
      newformSubj = subjectThesis;
    }
    //prints out the action sequence with either "It" or the subject
    System.out.println(newformSubj + " used " + obj("") + " and " + PastTenseV("") + " " + obj("") + " at the " + adj("") + " " + SubjectN("") + ".");
  
  }  

  public static String conclusion(String subjectThesis) { //conclusion sequence that takes the subject of thesis into its sentence
    System.out.println("That " + subjectThesis + " " + PastTenseV("") + " their " + obj("") + "!");
    return subjectThesis;
  }

  public static void paragraph() {    //runs thesis, action sequence, and conclusion
    String a = thesis();  //runs thesis() and assigns the return as a, to be carried over into action sequence and conclusion
    int actSent = (int) (Math.random()*10)+1;   //does at least one action sequence, more at random
    int i = 0;
    do {                                        // a do while loop which runs actionSeq() up to the random number
      actionSeq(a);
      i++;
    } while (i <= actSent);
    conclusion(a);                              //runs conclusion sequence with a as an input
  }

  public static void main (String[] args) {     //main method that runs the paragraph method
    paragraph();
  }
}